<?php

declare(strict_types=1);

namespace Drupal\entity_clone_entity_browser_block\EventSubscriber;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\entity_browser_block\Plugin\Block\EntityBrowserBlock;
use Drupal\entity_clone\Event\EntityCloneEvent;
use Drupal\entity_clone\Event\EntityCloneEvents;
use Drupal\layout_builder\SectionComponent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Clone inline blocks during an entity clone.
 */
class EntityCloneSubscriber implements EventSubscriberInterface {

  /**
   * Constructs EntityCloneSubscriber object.
   */
  public function __construct(protected UuidInterface $uuidService, protected EntityTypeManagerInterface $entityTypeManager) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      EntityCloneEvents::POST_CLONE => 'postClone',
    ];
  }

  /**
   * Act to a post clone event.
   *
   * @param \Drupal\entity_clone\Event\EntityCloneEvent $event
   *   Clone event.
   */
  public function postClone(EntityCloneEvent $event): void {
    // Check the on/off switch on the clone form.
    if (!$event->getProperties()['clone_entity_browser_block']) {
      return;
    }

    $entity = $event->getClonedEntity();

    // Find any layout builder fields.
    if ($entity instanceof FieldableEntityInterface) {
      foreach ($entity->getFieldDefinitions() as $field_id => $field_definition) {
        if ($this->fieldIsLayoutBuilder($field_definition)) {
          $field = $entity->get($field_id);
          $sections = $field->getSections();
          foreach ($sections as $delta => $section) {
            $components = $section->getComponents();
            foreach ($components as $uuid => $component) {
              $plugin = $component->getPlugin();
              if ($plugin instanceof EntityBrowserBlock) {
                // Duplicate the block, update the reference.
                $cloned_component = $this->cloneComponent($component);
                $sections[$delta]->insertAfterComponent($uuid, $cloned_component);
                $sections[$delta]->removeComponent($uuid);
              }
            }
          }
          $entity->set($field_id, $sections);
        }
      }
      $entity->save();
    }
  }

  /**
   * Validate if current field is layout section field.
   */
  protected function fieldIsLayoutBuilder($field_definition): bool {
    return ($field_definition->getType() == 'layout_section');
  }

  /**
   * Clone a entity_browser_block component.
   *
   * @param \Drupal\layout_builder\SectionComponent $component
   *   Section component object.
   *
   * @return \Drupal\layout_builder\SectionComponent
   *   Cloned section component.
   */
  protected function cloneComponent(SectionComponent $component): SectionComponent {
    $plugin = $component->getPlugin();
    $cloned_block_entity_ids = $this->cloneBlockContent($plugin->getConfiguration()['entity_ids']);
    $configuration = $component->toArray()['configuration'];
    // Only new thing is a new block revision.
    $configuration['entity_ids'] = $cloned_block_entity_ids;
    return new SectionComponent($this->uuidService->generate(), $component->getRegion(), $configuration);
  }

  /**
   * Clone block content.
   *
   * @param array $entity_ids
   *   Array of all the entity ids in "entity_type_id:entity_id" format.
   *
   * @return array
   *   Array of all the cloned entities in "entity_type_id:entity_id" format.
   */
  protected function cloneBlockContent(array $entity_ids): array {
    $cloned_entities = [];
    if (empty($entity_ids)) {
      return [];
    }
    foreach ($entity_ids as $entity) {
      [$entity_type_id, $entity_id] = explode(':', $entity);
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
      $cloned_entity = $entity->createDuplicate();

      // Update the unique description for the block content entity.
      if ($cloned_entity instanceof BlockContent) {
        $cloned_entity->setInfo('Clone of ' . $cloned_entity->get('info')->getString());
      }
      $cloned_entity->setNewRevision();
      $cloned_entity->save();
      $cloned_entities[] = $entity_type_id . ':' . $cloned_entity->id();
    }
    return $cloned_entities;
  }

}
