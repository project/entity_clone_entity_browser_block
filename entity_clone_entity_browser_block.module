<?php

/**
 * @file
 * Contains entity_clone_entity_browser_block.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function entity_clone_entity_browser_block_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the entity_clone_inline_blocks module.
    case 'help.page.entity_clone_inline_blocks':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Handle duplication of blocks created via Entity Browser Block plugins.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function entity_clone_entity_browser_block_form_entity_clone_form_alter(&$form, FormStateInterface $form_state, $form_id): void {
  // This is the on/off switch for this module.
  $form['clone_entity_browser_block'] = [
    '#type' => 'checkbox',
    '#title' => t('Clone entity browser blocks'),
    '#description' => t('Duplicate any Layout Builder blocks created via Entity Browser Block plugins, rather than copying references.'),
    '#default_value' => TRUE,
    '#weight' => 97,
  ];
  $form['clone']['#weight'] = 98;
  $form['abort']['#weight'] = 99;
}
